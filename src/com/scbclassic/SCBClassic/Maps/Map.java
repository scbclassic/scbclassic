package com.scbclassic.SCBClassic.Maps;

import org.bukkit.Location;

import java.util.HashMap;
import java.util.List;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public interface Map {

    public String getName();

    public HashMap<Integer, List<Location>> getLivesBoards();

    public Location getLobbyLocation();

    public List<Location> getSpawnLocations();

    public List<Location> getDropLocations();

}
