package com.scbclassic.SCBClassic.Maps.GameMaps;

import com.scbclassic.SCBClassic.Main;
import com.scbclassic.SCBClassic.Maps.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class CandyLand implements Map {

    @Override
    public String getName() {
        return "CandyLand";
    }

    HashMap<Integer, List<Location>> data = new HashMap<Integer, List<Location>>();
    {
        data.put(0, Arrays.asList(
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -29),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -28),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -27),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -26),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -25)
        ));
        data.put(1, Arrays.asList(
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -21),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -20),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -19),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -18),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -17)
        ));
        data.put(2, Arrays.asList(
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -13),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -12),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -11),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -10),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -9)
        ));
        data.put(3, Arrays.asList(
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -5),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -4),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -3),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -2),
                new Location(Bukkit.getWorld(Main.getWorldName()), -553, 142, -1)
        ));
    }

    @Override
    public HashMap<Integer, List<Location>> getLivesBoards() {
        return data;
    }

    @Override
    public Location getLobbyLocation() {
        return new Location(Bukkit.getWorld(Main.getWorldName()), -538, 132, -12);
    }

    List<Location> spawns = new ArrayList<Location>();
    {
        spawns.add(new Location(Bukkit.getWorld(Main.getWorldName()), -575, 132, -14));
        spawns.add(new Location(Bukkit.getWorld(Main.getWorldName()), -592, 141, 0));
        spawns.add(new Location(Bukkit.getWorld(Main.getWorldName()), -598, 142, -16));
        spawns.add(new Location(Bukkit.getWorld(Main.getWorldName()), -588, 144, -31));
    }

    @Override
    public List<Location> getSpawnLocations() {
        return spawns;
    }

    @Override
    public List<Location> getDropLocations() {
        return null;
    }

}
