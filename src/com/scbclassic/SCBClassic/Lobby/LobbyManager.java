package com.scbclassic.SCBClassic.Lobby;

import com.scbclassic.SCBClassic.Games.Game;
import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class LobbyManager {

    public void updateSign(Game g) {
        Block b = Bukkit.getWorld(Main.getWorldName()).getBlockAt(g.getSignLocation());
        if(b.getType() == Material.WALL_SIGN) {
            Sign sign = (Sign) b.getState();
            sign.setLine(0, GameManager.getInstance().getState(g).getFormattedName());
            sign.setLine(1, g.getMap().getName());
            if(GameManager.getInstance().getState(g) == GameManager.GameState.RUNNING) {
                sign.setLine(2, "--Players--");
                sign.setLine(3, "Alive-"+g.getPlayersAlive().size()+"|Dead-"+(4-g.getPlayersAlive().size()));
            } else {
                sign.setLine(2, "Players: " + GameManager.getInstance().getPlayers(g).size() + "/4");
                sign.setLine(3, g.getStartTime() + "s");
            }
            sign.update(true);
        }
    }

    public void updateSign(String game) {
        updateSign(GameManager.getInstance().forName(game));
    }

    public void teleportToSpawn(Player p) {
        Location loc = new Location(Bukkit.getWorld(Main.getWorldName()), -2.50, 150, -0.50);
        loc.setYaw(270);
        loc.setPitch(3);
        p.teleport(loc);
    }

}
