package com.scbclassic.SCBClassic.Games.Games;

import com.scbclassic.SCBClassic.Games.Game;
import com.scbclassic.SCBClassic.Main;
import com.scbclassic.SCBClassic.Maps.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class CandyLand extends Game {

    @Override
    public Map getMap() {
        return new com.scbclassic.SCBClassic.Maps.GameMaps.CandyLand();
    }

    @Override
    public Location getSignLocation() {
        return new Location(Bukkit.getWorld(Main.getWorldName()), -21, 152, -2);
    }

}
