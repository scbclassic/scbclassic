package com.scbclassic.SCBClassic.Games;

import com.scbclassic.SCBClassic.Classes.ClassManager;
import com.scbclassic.SCBClassic.Classes.GameClasses.Wither;
import com.scbclassic.SCBClassic.DoubleJump.DoubleJumpManager;
import com.scbclassic.SCBClassic.Lobby.LobbyManager;
import com.scbclassic.SCBClassic.Main;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import com.scbclassic.SCBClassic.Player.Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class GameManager {

    protected static GameManager manager = new GameManager();
    private LobbyManager lobbyManager = new LobbyManager();
    private HashMap<Game, List<Player>> players = new HashMap<Game, List<Player>>();
    private HashMap<Game, GameState> games = new HashMap<Game, GameState>();

    public void addGame(Game g) {
        players.put(g, new ArrayList<Player>());
        games.put(g, GameState.LOBBY);
        System.out.println("Successfully loaded game " + g.getMap().getName() + "!");
    }

    public void removeGame(Game g) {
        if(games.containsKey(g)) {
            games.remove(g);
        }
        if(players.containsKey(g)) {
            players.remove(g);
        }
    }

    public Game forName(String name) {
        for(Game g : games.keySet()) {
            if(g.getMap().getName().equals(name)) {
                return g;
            }
        }
        return null;
    }

    public Collection<Game> getGames() {
        return games.keySet();
    }

    public Game getGame(Game g) {
        return forName(g.toString());
    }

    public List<Player> getPlayers(Game g) {
        return players.get(g);
    }

    public void setState(Game g, GameState s) {
        games.put(g, s);
    }

    public GameState getState(Game g) {
        return games.get(g);
    }

    public void joinGame(Game g, Player p) {
        PlayerManager.getInstance().getPlayer(p).setCurrentGame(g);
        p.teleport(g.getMap().getLobbyLocation());

        List<Player> playerList = players.get(g);
        {
            playerList.add(p);
        }
        players.put(g, playerList);

        p.sendMessage(ChatColor.GREEN + "You have joined " + ChatColor.YELLOW + g.getMap().getName() + ChatColor.GREEN + ". Choose your class by clicking on one of the signs!");

        lobbyManager.updateSign(g);

        if(players.get(g).size() == 4) {
            startGame(g);
        }
    }

    public void leaveGame(Game g, Player p) {
        lobbyManager.teleportToSpawn(p);

        List<Player> playerList = players.get(g);
        {
            playerList.remove(p);
        }
        players.put(g, playerList);

        PlayerManager.getInstance().getPlayer(p).setCurrentGame(null);

        p.sendMessage(ChatColor.GREEN + "You have left the game and returned to the main lobby.");
    }

    public void clearPlayers(Game g) {
        List<Player> playerList = players.get(g);
        {
            playerList.clear();
        }
        players.put(g, playerList);
    }

    public void startGame(final Game g) {
        setState(g, GameState.STARTING);
        new BukkitRunnable() {
            int t = 31;
            public void run() {
                if(getPlayers(g).size() == 4) {
                    t--;
                    g.setStartTime(t);
                    lobbyManager.updateSign(g);
                    if (t == 30 || t == 15 || t == 5) {
                        for (Player p : getPlayers(g)) {
                            p.sendMessage(ChatColor.GREEN + "The game will begin in " + ChatColor.YELLOW + t + " seconds" + ChatColor.GREEN + "!");
                        }
                    } else if (t == 0) {
                        for (int i=0; i < getPlayers(g).size(); i++) {
                            getPlayers(g).get(i).teleport(g.getMap().getSpawnLocations().get(i));
                        }
                        for (Player p : getPlayers(g)) {
                            if (g.getPlayerClasses().get(p) != null) {
                                for (int slot : g.getPlayerClasses().get(p).getItems().keySet()) {
                                    p.getInventory().setItem(slot, g.getPlayerClasses().get(p).getItems().get(slot));
                                }
                            } else {
                                g.getPlayerClasses().put(p, ClassManager.getInstance().forName("Wither"));
                                for (int slot : ClassManager.getInstance().forName("Wither").getItems().keySet()) {
                                    p.getInventory().setItem(slot, ClassManager.getInstance().forName("Wither").getItems().get(slot));
                                }
                            }
                            DoubleJumpManager.getInstance().setDoubleJump(p, true);
                        }
                        for (int i=0; i < getPlayers(g).size(); i++) {
                            g.getPlayerData().put(getPlayers(g).get(i), i);
                            g.setPlayerLives(i, 5);
                            getPlayers(g).get(i).setLevel(i);
                        }

                        setState(g, GameState.RUNNING);
                        g.setStartTime(30);
                        lobbyManager.updateSign(g);
                        cancel();
                    }
                } else {
                    cancel();
                }
            }
        }.runTaskTimer(Main.getInstance(), 20L, 20L);
    }

    public static GameManager getInstance() {
        return manager;
    }

    public enum GameState {
        LOBBY("Lobby", ChatColor.RED), STARTING("Lobby", ChatColor.RED), RUNNING("In-Game", ChatColor.GREEN);

        private String name;
        private ChatColor color;

        GameState(String name, ChatColor color) {
            this.name = name;
            this.color = color;
        }

        public String getFormattedName() {
            return color + name;
        }

        public String getName() {
            return name;
        }

        public ChatColor getColor() {
            return color;
        }

    }

}
