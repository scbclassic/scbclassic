package com.scbclassic.SCBClassic.Games;

import com.scbclassic.SCBClassic.Classes.SCBClass;
import com.scbclassic.SCBClassic.Main;
import com.scbclassic.SCBClassic.Maps.Map;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public abstract class Game {

    private HashMap<Player, Integer> playerData = new HashMap<Player, Integer>();
    private HashMap<Player, SCBClass> playerClasses = new HashMap<Player, SCBClass>();
    private HashMap<Integer, Integer> lives = new HashMap<Integer, Integer>();
    private int startTime = 30;

    public abstract Map getMap();

    public abstract Location getSignLocation();

    public HashMap<Player, SCBClass> getPlayerClasses() {
        return playerClasses;
    }

    public HashMap<Integer, Integer> getPlayerLives() {
        return lives;
    }

    public HashMap<Player, Integer> getPlayerData() {
        return playerData;
    }

    public void setPlayerLives(int player, int lives) {
        if(lives - 5 > 5) return;
        Block one = Bukkit.getWorld(Main.getWorldName()).getBlockAt(getMap().getLivesBoards().get(player).get(0));
        Block two = Bukkit.getWorld(Main.getWorldName()).getBlockAt(getMap().getLivesBoards().get(player).get(1));
        Block three = Bukkit.getWorld(Main.getWorldName()).getBlockAt(getMap().getLivesBoards().get(player).get(2));
        Block four = Bukkit.getWorld(Main.getWorldName()).getBlockAt(getMap().getLivesBoards().get(player).get(3));
        Block five = Bukkit.getWorld(Main.getWorldName()).getBlockAt(getMap().getLivesBoards().get(player).get(4));
        switch (lives) {
            case 0:
                one.setType(Material.CLAY);
                two.setType(Material.CLAY);
                three.setType(Material.CLAY);
                four.setType(Material.CLAY);
                five.setType(Material.CLAY);
                break;
            case 1:
                one.setType(Material.GLOWSTONE);
                two.setType(Material.CLAY);
                three.setType(Material.CLAY);
                four.setType(Material.CLAY);
                five.setType(Material.CLAY);
                break;
            case 2:
                one.setType(Material.GLOWSTONE);
                two.setType(Material.GLOWSTONE);
                three.setType(Material.CLAY);
                four.setType(Material.CLAY);
                five.setType(Material.CLAY);
                break;
            case 3:
                one.setType(Material.GLOWSTONE);
                two.setType(Material.GLOWSTONE);
                three.setType(Material.GLOWSTONE);
                four.setType(Material.CLAY);
                five.setType(Material.CLAY);
                break;
            case 4:
                one.setType(Material.GLOWSTONE);
                two.setType(Material.GLOWSTONE);
                three.setType(Material.GLOWSTONE);
                four.setType(Material.GLOWSTONE);
                five.setType(Material.CLAY);
                break;
            case 5:
                one.setType(Material.GLOWSTONE);
                two.setType(Material.GLOWSTONE);
                three.setType(Material.GLOWSTONE);
                four.setType(Material.GLOWSTONE);
                five.setType(Material.GLOWSTONE);
                break;
        }
        getPlayerLives().put(player, lives);
    }

    public List<Player> getPlayersAlive() {
        List<Player> data = new ArrayList<Player>();
        {
            for(Player p : GameManager.getInstance().getPlayers(this)) {
                if(getPlayerLives().get(getPlayerData().get(p)) > 0) {
                    data.add(p);
                }
            }
        }
        return data;
    }

    public void spectate(Player p, Game g) {
        for(Player pl : GameManager.getInstance().getPlayers(this)) {
            pl.hidePlayer(p);
        }
        p.setAllowFlight(true);
        p.setFlying(true);
        p.spigot().setCollidesWithEntities(false);
        PlayerManager.getInstance().getPlayer(p).setSpectatingGame(g);
        p.teleport(g.getMap().getSpawnLocations().get(new Random().nextInt(3)));
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public void selectClass(Player p, SCBClass c) {
        p.sendMessage(ChatColor.GREEN + "You have chosen the character " + c.getNameWithColor() + ChatColor.GREEN + "!");
        playerClasses.put(p, c);
    }

}
