package com.scbclassic.SCBClassic;

import com.scbclassic.SCBClassic.Classes.ClassManager;
import com.scbclassic.SCBClassic.Classes.GameClasses.Wither;
import com.scbclassic.SCBClassic.Games.Game;
import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Games.Games.CandyLand;
import com.scbclassic.SCBClassic.Listeners.*;
import com.scbclassic.SCBClassic.Lobby.LobbyManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class Main extends JavaPlugin {

    protected static Main instance;

    public void onEnable() {
        instance = this;
        buildServer();
    }

    public void onDisable() {
        instance = null;
    }

    public void buildServer() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        {
            pm.registerEvents(new JoinListener(), this);
            pm.registerEvents(new ChatListener(), this);
            pm.registerEvents(new QuitListener(), this);
            pm.registerEvents(new DisabledActionsListeners(), this);
            pm.registerEvents(new GameSignClickListener(), this);
            pm.registerEvents(new PregameLobbySignClickListener(), this);
            pm.registerEvents(new DeathListener(), this);
        }
        {
            GameManager.getInstance().addGame(new CandyLand());
        }
        {
            ClassManager.getInstance().registerClass(new Wither());
        }
        {
            for(Game g : GameManager.getInstance().getGames()) {
                new LobbyManager().updateSign(g);
            }
        }
    }

    public static Main getInstance() {
        return instance;
    }

    public static String getWorldName() {
        return "world";
    }

}
