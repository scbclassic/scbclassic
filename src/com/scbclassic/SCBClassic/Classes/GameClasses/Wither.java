package com.scbclassic.SCBClassic.Classes.GameClasses;

import com.scbclassic.SCBClassic.Classes.ClassManager;
import com.scbclassic.SCBClassic.Classes.SCBClass;
import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class Wither implements SCBClass {

    private HashMap<Integer, ItemStack> items = new HashMap<Integer, ItemStack>();

    public Wither() {
        {
            ItemStack item = new ItemStack(Material.NETHER_STAR, 1);
            item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
            item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
            items.put(1, item);
        }
        {
            ItemStack item = new ItemStack(Material.BOW, 1);
            item.addEnchantment(Enchantment.ARROW_INFINITE, 1);
            items.put(0, item);
        }
        {
            //TODO - ADD SPLASH POTIONS
        }
        {
            ItemStack item = new ItemStack(Material.ARROW, 1);
            items.put(9, item);
        }
    }

    @Override
    public String getName() {
        return "Wither";
    }

    @Override
    public String getNameWithColor() {
        return ChatColor.DARK_GRAY + "Wither";
    }

    @Override
    public ClassManager.ClassType getType() {
        return ClassManager.ClassType.DEFAULT;
    }

    @Override
    public double getDoubleJumpHeight() {
        return 0.9D;
    }

    @Override
    public HashMap<Integer, ItemStack> getItems() {
        return items;
    }

    @EventHandler
    public void shootingHeads(EntityShootBowEvent e) {
        if(e.getEntity() instanceof Player) {
            if(PlayerManager.getInstance().getPlayer((Player) e.getEntity()).getCurrentGame() != null) {
                if(PlayerManager.getInstance().getPlayer((Player) e.getEntity()).getCurrentGame().getPlayerClasses().get(e.getEntity()) == ClassManager.getInstance().forName("Wither")) {
                    e.setCancelled(true);
                    e.getEntity().launchProjectile(WitherSkull.class).setVelocity(e.getEntity().getLocation().getDirection());
                }
            }
        }
    }

    @EventHandler
    public void onSkullHit(EntityExplodeEvent e) {
        if(e.getEntity().getType() == EntityType.WITHER_SKULL) {
            e.blockList().clear();
        }
    }

}
