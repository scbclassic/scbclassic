package com.scbclassic.SCBClassic.Classes;

import com.scbclassic.SCBClassic.Main;
import com.scbclassic.SCBClassic.Player.SCBPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class ClassManager {

    private static ClassManager clazz = new ClassManager();
    private List<SCBClass> classes = new ArrayList<SCBClass>();

    public void registerClass(SCBClass c) {
        Main.getInstance().getServer().getPluginManager().registerEvents(c, Main.getInstance());
        classes.add(c);
    }

    public SCBClass forName(String name) {
        for(SCBClass c : classes) {
            if(c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

    public static ClassManager getInstance() {
        return clazz;
    }

    public enum ClassType {
        DEFAULT, PRO;
    }

}
