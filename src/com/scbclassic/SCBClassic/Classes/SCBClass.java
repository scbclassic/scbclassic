package com.scbclassic.SCBClassic.Classes;

import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public interface SCBClass extends Listener {

    public String getName();

    public String getNameWithColor();

    public ClassManager.ClassType getType();

    public double getDoubleJumpHeight();

    public HashMap<Integer, ItemStack> getItems();

}
