package com.scbclassic.SCBClassic.Player.Rank;

import org.bukkit.ChatColor;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public enum Rank {

    DEFAULT("", ChatColor.GRAY), VIP("VIP", ChatColor.GOLD), GM("GM", ChatColor.RED), OP("Op", ChatColor.RED);

    private String name;
    private ChatColor color;

    Rank(String name, ChatColor color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }21

    public ChatColor getColor() {
        return color;
    }

    public String getFormattedTag() {
        return (name.equals("") ? "" : ChatColor.WHITE + "[" + color + name + ChatColor.WHITE + "]");
    }

}
