package com.scbclassic.SCBClassic.Player;

import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class PlayerManager {

    protected static PlayerManager manager = new PlayerManager();
    private HashMap<Player, SCBPlayer> players = new HashMap<Player, SCBPlayer>();

    public void addPlayer(Player n, SCBPlayer player) {
        players.put(n, player);
    }

    public SCBPlayer getPlayer(Player p) {
        return players.get(p);
    }

    public void removePlayer(Player p) {
        players.remove(p);
    }

    public static PlayerManager getInstance() {
        return manager;
    }

}
