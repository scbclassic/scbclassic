package com.scbclassic.SCBClassic.Player;

import com.scbclassic.SCBClassic.Games.Game;
import com.scbclassic.SCBClassic.Player.Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.sql.PreparedStatement;
import java.util.UUID;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class SCBPlayer {

    private String name;
    private UUID uuid;
    private Game currentGame = null;
    private Game spectatingGame = null;
    private Rank rank = Rank.DEFAULT;

    public SCBPlayer(String name) {
        this.name = name;
        this.uuid = Bukkit.getOfflinePlayer(name).getUniqueId();
        {
            PreparedStatement stmt = null;
        }
    }

    public void save() {

    }

    public String getRankedTag(boolean withClass) {
        if(withClass) {
            String tag;
            {
                if(getCurrentGame().getPlayerClasses().get(Bukkit.getPlayer(getName())) != null) {
                    if(getRank() != Rank.DEFAULT) {
                        tag = ChatColor.WHITE + "[" + getCurrentGame().getPlayerClasses().get(Bukkit.getPlayer(name)).getNameWithColor()
                                + ChatColor.WHITE + "] " + getRank().getFormattedTag() + " " + ChatColor.WHITE + getName();
                    } else {
                        if(getCurrentGame().getPlayerClasses().get(Bukkit.getPlayer(name)) != null) {
                            tag = ChatColor.WHITE + "[" + getCurrentGame().getPlayerClasses().get(Bukkit.getPlayer(name)).getNameWithColor()
                                    + ChatColor.WHITE + "] " + getName();
                        } else {
                            tag = ChatColor.GRAY + name;
                        }
                    }
                } else {
                    if(getRank() != Rank.DEFAULT) {
                        tag = getRank().getFormattedTag() + " " + ChatColor.WHITE + getName();
                    } else {
                        tag = ChatColor.WHITE + getName();
                    }
                }
            }
            return tag;
        } else {
            return (getRank() != Rank.DEFAULT) ? getRank().getFormattedTag() + " " + ChatColor.WHITE + getName() : getRank().getFormattedTag() + ChatColor.GRAY + getName();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Game getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(Game currentGame) {
        this.currentGame = currentGame;
    }

    public Game getSpectatingGame() {
        return spectatingGame;
    }

    public void setSpectatingGame(Game spectatingGame) {
        this.spectatingGame = spectatingGame;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

}
