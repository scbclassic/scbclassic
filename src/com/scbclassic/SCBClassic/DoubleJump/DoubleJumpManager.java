package com.scbclassic.SCBClassic.DoubleJump;

import com.scbclassic.SCBClassic.Games.Game;
import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Main;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class DoubleJumpManager implements Listener {

    protected static DoubleJumpManager manager = new DoubleJumpManager();
    private List<Player> dj = new ArrayList<Player>();

    public DoubleJumpManager() {
        Bukkit.getServer().getPluginManager().registerEvents(this, Main.getInstance());
    }

    public void setDoubleJump(Player p, boolean b) {
        if(!b) {
            if(dj.contains(p)) {
                dj.remove(p);
            }
            if(p.getGameMode() != GameMode.CREATIVE) {
                p.setAllowFlight(false);
            }
        } else {
            if(!dj.contains(p)) {
                dj.add(p);
            }
            p.setAllowFlight(true);
        }
    }

    @EventHandler
    public void onToggleFlight(PlayerToggleFlightEvent e) {
        if(e.getPlayer().getGameMode() != GameMode.CREATIVE) {
            if (dj.contains(e.getPlayer())) {
                if (PlayerManager.getInstance().getPlayer(e.getPlayer()).getCurrentGame() != null) {
                    if(PlayerManager.getInstance().getPlayer(e.getPlayer()).getCurrentGame().getPlayerClasses().get(e.getPlayer()) != null) {
                        e.getPlayer().setVelocity(e.getPlayer().getLocation().getDirection().multiply(0.15D).setY(
                                PlayerManager.getInstance().getPlayer(e.getPlayer()).getCurrentGame().getPlayerClasses().get(e.getPlayer()).getDoubleJumpHeight()));
                    } else {
                        e.getPlayer().setVelocity(e.getPlayer().getLocation().getDirection().multiply(0.15D).setY(0.7D));
                    }
                } else {
                    e.getPlayer().setVelocity(e.getPlayer().getLocation().getDirection().multiply(0.15D).setY(0.7D));
                }
                e.getPlayer().playSound(e.getPlayer().getLocation(), getRandomSound(), 0.2F, 2F);

                e.getPlayer().setAllowFlight(false);

                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if(e.getPlayer().getGameMode() != GameMode.CREATIVE) {
            if (dj.contains(e.getPlayer())) {
                if (e.getPlayer().getLocation().getBlock().getRelative(BlockFace.DOWN).getType() != Material.AIR) {
                    e.getPlayer().setAllowFlight(true);
                }
            }
        }
    }

    private Sound getRandomSound() {
        return Sound.AMBIENCE_CAVE;
    }

    public static DoubleJumpManager getInstance() {
        return manager;
    }

}
