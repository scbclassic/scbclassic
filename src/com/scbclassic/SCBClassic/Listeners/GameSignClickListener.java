package com.scbclassic.SCBClassic.Listeners;

import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import com.scbclassic.SCBClassic.Player.Rank.Rank;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class GameSignClickListener implements Listener {

    @EventHandler
    public void onSignClick(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if(e.getClickedBlock().getType() == Material.WALL_SIGN) {
                Player p = e.getPlayer();
                Sign sign = (Sign) e.getClickedBlock().getState();
                if(sign.getLine(0).equals(ChatColor.RED + "Lobby")) {
                    if(GameManager.getInstance().getState(GameManager.getInstance().forName(ChatColor.stripColor(sign.getLine(1)))) == GameManager.GameState.LOBBY) {
                        GameManager.getInstance().joinGame(GameManager.getInstance().forName(ChatColor.stripColor(sign.getLine(1))), e.getPlayer());
                    } else if(PlayerManager.getInstance().getPlayer(e.getPlayer()).getRank() != Rank.DEFAULT) {
                        p.sendMessage(ChatColor.RED + "You cannot spectate at this time.");
                    } else {
                        p.sendMessage(ChatColor.RED + "You cannot join at this time.");
                    }
                } else if(sign.getLine(0).equals(ChatColor.GREEN + "In-Game")) {
                    if(PlayerManager.getInstance().getPlayer(e.getPlayer()).getRank() != Rank.DEFAULT) {
                        GameManager.getInstance().forName(ChatColor.stripColor(sign.getLine(1))).spectate(e.getPlayer(), GameManager.getInstance().forName(ChatColor.stripColor(sign.getLine(1))));
                    } else {
                        p.sendMessage(ChatColor.RED + "Please purchase VIP to spectate.");
                    }
                }
            }
        }
    }

}
