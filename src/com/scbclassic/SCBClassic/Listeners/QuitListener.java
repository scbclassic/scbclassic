package com.scbclassic.SCBClassic.Listeners;

import com.scbclassic.SCBClassic.DoubleJump.DoubleJumpManager;
import com.scbclassic.SCBClassic.Games.Game;
import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Lobby.LobbyManager;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import com.scbclassic.SCBClassic.Player.Rank.Rank;
import com.scbclassic.SCBClassic.Player.SCBPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        SCBPlayer scbPlayer = PlayerManager.getInstance().getPlayer(e.getPlayer());
        Game g = scbPlayer.getCurrentGame();
        {
            if (scbPlayer.getCurrentGame() != null) {
                if(g.getPlayersAlive().size()+1 > 2) {
                    if(g.getPlayerClasses().containsKey(e.getPlayer())) g.getPlayerClasses().remove(e.getPlayer());
                    if(g.getPlayerLives().containsKey(g.getPlayerData().get(e.getPlayer()))) g.getPlayerLives().put(g.getPlayerData().get(e.getPlayer()), 0);
                    e.getPlayer().getInventory().clear();
                    e.getPlayer().teleport(g.getMap().getLobbyLocation());
                    e.getPlayer().sendMessage(ChatColor.RED + "You have lost! Wait in the lobby until the game is over or go back to the main lobby to join another game.");
                } else {
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        if(PlayerManager.getInstance().getPlayer(p).getCurrentGame() == null) {
                            p.sendMessage(PlayerManager.getInstance().getPlayer(g.getPlayersAlive().get(0)).getRankedTag(true) + ChatColor.GREEN + " just won on " + ChatColor.YELLOW + g.getMap().getName() + ChatColor.GREEN + "!");
                        }
                    }
                    for(Player p : GameManager.getInstance().getPlayers(g)) {
                        if(PlayerManager.getInstance().getPlayer(p).getRank() == Rank.DEFAULT) DoubleJumpManager.getInstance().setDoubleJump(p, false);
                        p.getInventory().clear();
                        new LobbyManager().teleportToSpawn(p);
                        p.sendMessage(PlayerManager.getInstance().getPlayer(g.getPlayersAlive().get(0)).getRankedTag(true) + ChatColor.GREEN + " has won!");
                        PlayerManager.getInstance().getPlayer(p).setCurrentGame(null);
                    }

                    g.getPlayerClasses().clear();
                    g.getPlayerLives().clear();
                    g.getPlayerData().clear();
                    GameManager.getInstance().clearPlayers(g);
                    GameManager.getInstance().setState(g, GameManager.GameState.LOBBY);
                }

                new LobbyManager().updateSign(g);
                GameManager.getInstance().leaveGame(g, e.getPlayer());
            } else if(scbPlayer.getSpectatingGame() != null) {
                scbPlayer.setSpectatingGame(null);
            }
        }
        PlayerManager.getInstance().removePlayer(e.getPlayer());
    }

}
