package com.scbclassic.SCBClassic.Listeners;

import com.scbclassic.SCBClassic.DoubleJump.DoubleJumpManager;
import com.scbclassic.SCBClassic.Lobby.LobbyManager;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import com.scbclassic.SCBClassic.Player.Rank.Rank;
import com.scbclassic.SCBClassic.Player.SCBPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class JoinListener implements Listener {

    @EventHandler
    public void onPreJoin(AsyncPlayerPreLoginEvent e) {
        //TODO - BANS
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        SCBPlayer p = new SCBPlayer(e.getPlayer().getName());
        PlayerManager.getInstance().addPlayer(e.getPlayer(), p);
        /**
         * TEMPORARY OP!!!!
         */
        if(e.getPlayer().isOp()) {
            PlayerManager.getInstance().getPlayer(e.getPlayer()).setRank(Rank.OP);
        }

        new LobbyManager().teleportToSpawn(e.getPlayer());

        if(p.getRank() != Rank.DEFAULT) {
            DoubleJumpManager.getInstance().setDoubleJump(e.getPlayer(), true);
        }

        /**
         * To stop the player from being invis after a relog during spec
         */
        for(Player pl : Bukkit.getOnlinePlayers()) {
            pl.showPlayer(e.getPlayer());
        }

        e.getPlayer().setFoodLevel(20);
        e.getPlayer().setHealth(20);
        e.getPlayer().getInventory().clear();
        e.getPlayer().getInventory().setArmorContents(null);
        e.getPlayer().spigot().setCollidesWithEntities(true);
        e.getPlayer().setLevel(0);
    }

}
