package com.scbclassic.SCBClassic.Listeners;

import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import com.scbclassic.SCBClassic.Player.SCBPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        SCBPlayer scbPlayer = PlayerManager.getInstance().getPlayer(e.getPlayer());
        e.setCancelled(true);
        if(scbPlayer.getCurrentGame() != null) {
            for (Player p : GameManager.getInstance().getPlayers(scbPlayer.getCurrentGame())) {
                p.sendMessage(scbPlayer.getRankedTag(true) + ChatColor.GRAY + ": " + e.getMessage());
            }
        } else if(scbPlayer.getSpectatingGame() != null) {
            for(Player p : Bukkit.getOnlinePlayers()) {
                if(PlayerManager.getInstance().getPlayer(p).getSpectatingGame() == scbPlayer.getSpectatingGame()) {
                    p.sendMessage(scbPlayer.getRankedTag(false) + ChatColor.GRAY + ": " + e.getMessage());
                }
            }
        } else {
            for(Player p : Bukkit.getOnlinePlayers()) {
                if(PlayerManager.getInstance().getPlayer(p).getCurrentGame() == null) {
                    p.sendMessage(scbPlayer.getRankedTag(false) + ChatColor.GRAY + ": " + e.getMessage());
                }
            }
        }
    }

}
