package com.scbclassic.SCBClassic.Listeners;

import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class PregameLobbySignClickListener implements Listener {

    @EventHandler
    public void onSignClick(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.WALL_SIGN) {
                Player p = e.getPlayer();
                Sign sign = (Sign) e.getClickedBlock().getState();
                if (sign.getLine(0).equals("Click to Go") && sign.getLine(1).equals(ChatColor.BOLD + "Back to Lobby")) {
                    if (PlayerManager.getInstance().getPlayer(e.getPlayer()).getCurrentGame() != null) {
                        GameManager.getInstance().leaveGame(PlayerManager.getInstance().getPlayer(e.getPlayer()).getCurrentGame(), p);
                    }
                }
            }
        }
    }

}
