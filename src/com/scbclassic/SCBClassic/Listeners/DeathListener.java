package com.scbclassic.SCBClassic.Listeners;

import com.scbclassic.SCBClassic.DoubleJump.DoubleJumpManager;
import com.scbclassic.SCBClassic.Games.Game;
import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Lobby.LobbyManager;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import com.scbclassic.SCBClassic.Player.Rank.Rank;
import com.scbclassic.SCBClassic.Player.SCBPlayer;
import net.minecraft.server.v1_8_R2.PacketPlayInClientCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.Random;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class DeathListener implements Listener {

    @EventHandler
    public void onDeath(final PlayerDeathEvent e) {
        ((CraftPlayer) e.getEntity()).getHandle().playerConnection.a(new PacketPlayInClientCommand(PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN));
        if(PlayerManager.getInstance().getPlayer(e.getEntity()).getCurrentGame() != null) {
            Game g = PlayerManager.getInstance().getPlayer(e.getEntity()).getCurrentGame();
            SCBPlayer scbPlayer = PlayerManager.getInstance().getPlayer(e.getEntity());
            {
                e.setDeathMessage(null);

                g.setPlayerLives(e.getEntity().getLevel(), g.getPlayerLives().get(e.getEntity().getLevel()) - 1);

                int lives = g.getPlayerLives().get(g.getPlayerData().get(e.getEntity())); //error happening here

                String deathMsg = scbPlayer.getRankedTag(true) + ChatColor.RED + " was murdered via the dark arts.";
                {
                    EntityDamageEvent.DamageCause cause = e.getEntity().getLastDamageCause().getCause();
                    if (cause == EntityDamageEvent.DamageCause.VOID) {
                        deathMsg = scbPlayer.getRankedTag(true) + ChatColor.RED + " fell into the void.";
                    } else if (cause == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
                        if (e.getEntity().getKiller() != null) {
                            deathMsg = scbPlayer.getRankedTag(true) + ChatColor.RED + " was killed by "
                                    + PlayerManager.getInstance().getPlayer(e.getEntity().getPlayer()).getRankedTag(true) + ChatColor.RED + ".";
                        } else {
                            deathMsg = scbPlayer.getRankedTag(true) + ChatColor.RED + " was killed by a " + e.getEntity().getLastDamageCause().getEntity().getType().getName() + ChatColor.RED + ".";
                        }
                    }
                }
                String livesMsg;
                {
                    livesMsg = (lives != 0 ?
                            scbPlayer.getRankedTag(true) + ChatColor.RED + " has " + lives + " lives remaining." :
                            scbPlayer.getRankedTag(true) + ChatColor.RED + " has lost!");
                }
                for(Player p : GameManager.getInstance().getPlayers(scbPlayer.getCurrentGame())) {
                    p.sendMessage(deathMsg);
                    p.sendMessage(livesMsg);
                }

                if(lives == 0) {
                    if(g.getPlayersAlive().size()+1 > 2) {
                        g.getPlayerClasses().remove(e.getEntity());
                        e.getEntity().getInventory().clear();
                        e.getEntity().teleport(g.getMap().getLobbyLocation());
                        e.getEntity().sendMessage(ChatColor.RED + "You have lost! Wait in the lobby until the game is over or go back to the main lobby to join another game.");
                    } else {
                        String name = PlayerManager.getInstance().getPlayer(g.getPlayersAlive().get(0)).getRankedTag(true); //breaks when using it a second time

                        //specs/announce
                        for(Player p : Bukkit.getOnlinePlayers()) {
                            if(PlayerManager.getInstance().getPlayer(p).getSpectatingGame() == g) {
                                p.getInventory().clear();
                                new LobbyManager().teleportToSpawn(p);
                                p.sendMessage(PlayerManager.getInstance().getPlayer(g.getPlayersAlive().get(0)).getRankedTag(true) + ChatColor.GREEN + " has won!");
                                p.spigot().setCollidesWithEntities(true);
                                p.setFlying(false);
                                p.setAllowFlight(false);
                                PlayerManager.getInstance().getPlayer(p).setSpectatingGame(null);
                                if(PlayerManager.getInstance().getPlayer(p).getRank() != Rank.DEFAULT) DoubleJumpManager.getInstance().setDoubleJump(p, true);
                            } else if(PlayerManager.getInstance().getPlayer(p).getCurrentGame() == null) {
                                p.sendMessage(name + ChatColor.GREEN + " just won on " + ChatColor.YELLOW + g.getMap().getName() + ChatColor.GREEN + "!");
                            }
                        }

                        //in-game
                        for(Player p : GameManager.getInstance().getPlayers(g)) {
                            if(PlayerManager.getInstance().getPlayer(p).getRank() == Rank.DEFAULT) DoubleJumpManager.getInstance().setDoubleJump(p, false);
                            p.getInventory().clear();
                            new LobbyManager().teleportToSpawn(p);
                            p.setLevel(0);
                            p.sendMessage(name + ChatColor.GREEN + " has won!");
                            PlayerManager.getInstance().getPlayer(p).setCurrentGame(null);
                        }

                        g.getPlayerClasses().clear();
                        g.getPlayerLives().clear();
                        g.getPlayerData().clear();
                        GameManager.getInstance().clearPlayers(g);
                        GameManager.getInstance().setState(g, GameManager.GameState.LOBBY);
                    }

                    new LobbyManager().updateSign(g);
                } else {
                    e.getEntity().sendMessage(ChatColor.GREEN + "You have " + ChatColor.YELLOW + lives + " lives " + ChatColor.GREEN + "remaining.");
                    e.getEntity().teleport(g.getMap().getSpawnLocations().get(new Random().nextInt(3)));
                }
            }
        } else {
            e.setDeathMessage(e.getEntity().getDisplayName() + ChatColor.RED + " died.");
        }
    }

}
