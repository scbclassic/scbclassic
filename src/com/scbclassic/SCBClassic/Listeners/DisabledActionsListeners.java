package com.scbclassic.SCBClassic.Listeners;

import com.scbclassic.SCBClassic.Games.GameManager;
import com.scbclassic.SCBClassic.Lobby.LobbyManager;
import com.scbclassic.SCBClassic.Player.PlayerManager;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * ****************************************************************************************
 * All code contained within this document is sole property of WesJD. All rights reserved.*
 * Do NOT distribute/reproduce any of this code without permission from WesJD.            *
 * Not following this statement will result in a void of all agreements made.             *
 * Enjoy.                                                                                 *
 * ****************************************************************************************
 */
public class DisabledActionsListeners implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if(e.getPlayer().getGameMode() != GameMode.CREATIVE) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onFall(EntityDamageEvent e) {
        if(e.getEntity() instanceof Player) {
            if(e.getCause() == EntityDamageEvent.DamageCause.FALL) {
                e.setCancelled(true);
            } else if(e.getCause() == EntityDamageEvent.DamageCause.VOID && PlayerManager.getInstance().getPlayer((Player) e.getEntity()).getCurrentGame() == null) {
                e.setCancelled(true);
                new LobbyManager().teleportToSpawn((Player) e.getEntity());
            }
        }
    }

    @EventHandler
    public void onFoodLoss(FoodLevelChangeEvent e) {
        if(e.getEntity() instanceof Player) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onLiquidMove(BlockFromToEvent e) {
        if(e.getBlock().isLiquid()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockStateChange(LeavesDecayEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if(e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
            if (PlayerManager.getInstance().getPlayer((Player) e.getDamager()).getCurrentGame() == null || PlayerManager.getInstance().getPlayer((Player) e.getEntity()).getCurrentGame() == null) {
                e.setCancelled(true);
            } else if(GameManager.getInstance().getState(PlayerManager.getInstance().getPlayer((Player) e.getDamager()).getCurrentGame()) != GameManager.GameState.RUNNING) {
                e.setCancelled(true);
            }
        }
    }

}
